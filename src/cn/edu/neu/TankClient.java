package cn.edu.neu;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;

/**
 * i.这个类的作用是坦克游戏的主窗口
 * ii.@authordong
 */
public class TankClient extends Frame {
    /**
     * 整个坦克游戏的宽度
     */
    public static final int GAME_WIDTH = 800;
    public static final int GAME_HEIGHT = 600;
    Tank myTank = new Tank(300, 300, true,
            Tank.Direction.STOP, this);
    Blood b = new Blood();
    Wall w1 = new Wall(100, 200, 20, 150, this),
            w2 = new Wall(300, 100, 300, 20, this);
    List<Explode> explodes = new ArrayList<Explode>();
    List<Missile> missiles = new ArrayList<Missile>();
    List<Tank> tanks = new ArrayList<Tank>();
    //这是一张虚拟图片
    Image offScreenImage = null;

    //paint这个方法不需要被调用，一旦要被重画的时会被自动调用
    public void paint(Graphics g) {
//显示出容器中装了多少炮弹
/*
1.指明子弹-爆炸-坦克的数量
2.以及坦克的生命值
*/
        g.drawString("missilescount:" + missiles.size(),
                10, 50);
        g.drawString("explodescount:" + explodes.size(),
                10, 70);
        g.drawString("tankscount:" + tanks.size(), 10,
                90);
        g.drawString("tanklife:" + myTank.getLife(),
                10, 110);
        if (tanks.size() <= 0) {
            for (int i = 0; i < 5; i++) {
                tanks.add(new Tank(50 + 40 * (i + 1), 50,
                        false, Tank.Direction.D, this));
            }
        }
//将容器中的炮弹逐个画出来
        for (int i = 0; i < missiles.size(); i++) {
            Missile m = missiles.get(i);
            m.collidesWithTanks(tanks);
            m.collidesWithTank(myTank);
            m.collidesWithWall(w1);
            m.collidesWithWall(w2);
            m.draw(g);
//if(!m.isLive())missiles.remove(m);
//elsem.draw(g);
        }
        for (int i = 0; i < explodes.size(); i++) {
            Explode e = explodes.get(i);
            e.draw(g);
        }
        for (int i = 0; i < tanks.size(); i++) {
            Tank t = tanks.get(i);
            t.collidesWithWall(w1);
            t.collidesWithWall(w2);
            t.collidesWithTanks(tanks);
            t.draw(g);
        }
        myTank.draw(g);
        myTank.eat(b);
        w1.draw(g);
        w2.draw(g);
        b.draw(g);
    }

    public void update(Graphics g) {
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH,
                    GAME_HEIGHT);
        }
//拿到这个图片上的画笔
        Graphics gOffScreen = offScreenImage.getGraphics();
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.GREEN);
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        print(gOffScreen);
        g.drawImage(offScreenImage, 0, 0, null);
    }

    /**
     * 本方法用于显示坦克主窗口
     */
    public void launchFrame() {
        for (int i = 0; i < 10; i++) {
            tanks.add(new Tank(50 + 40 * (i + 1), 50, false,
                    Tank.Direction.D, this));
        }
        this.setLocation(300, 50);
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setTitle("TankWar");
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        this.setResizable(false);
        this.setBackground(Color.GREEN);
        this.addKeyListener(new KeyMonitor());
        setVisible(true);
        new Thread(new PaintThread()).start();
    }

    public static void main(String[] args) {
        TankClient tc = new TankClient();
        tc.launchFrame();
    }

    private class PaintThread implements Runnable {
        public void run() {
            while (true) {
                repaint();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class KeyMonitor extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            myTank.KyePressed(e);
        }

        public void keyReleased(KeyEvent e) {
            myTank.kyeReleased(e);
        }
    }
}